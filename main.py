import os
import time

#PyCryptodome
from Crypto.PublicKey import ElGamal, DSA
import dsa

L = 1024
N = 160


dsa = dsa.DSA()
dsa.L = L
dsa.N = N
dsa.key_gen()
message = "Hallo Welt"
signature = dsa.sign(message)
print("Hallo Welt (Echte Nachricht) =>", dsa.verify(message, signature[0], signature[1]))
print("Hallo Welt (Manipulierte Nachricht) =>", dsa.verify("Hallo Welt", signature[0], signature[1]))


print("\nGenerating other DSA keys...")
start = time.process_time()
dsa2_keys = DSA.generate(L)
end = time.process_time()
print("Other DSA keys:\t\t", (end - start))

print("\nGenerating ElGamal keys...")
start = time.process_time()
elgamal_keys = ElGamal.generate(L, randfunc=os.urandom)
end = time.process_time()
print("ElGamal keys:\t\t", (end - start))
