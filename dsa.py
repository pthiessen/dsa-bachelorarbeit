import hashlib
import random
import secrets
import time
from math import ceil
from sympy import mod_inverse, isprime
from hashlib import sha256


#Quelle der Algorithmusidee:
#https://csrc.nist.gov/pubs/fips/186-4/final

class DSA:
    def __init__(self):
        self.public_key = None
        self.private_key = None
        self.verbose = True
        self.L = 512
        self.N = 160
        self.seedlen = -1

    def key_gen(self):
        self.check_parameter()
        p, q = self.gen_primes(self.L, self.N, self.seedlen)

        if ((p - 1) % q) != 0:
            raise Exception("Faulty prime calculation")

        g = self.create_generator(p, q)
        if not self.validate_generator(p, q, g):
            raise Exception("Faulty generator")

        a = random.randint(1, q - 1)
        A = pow(g, a, p)
        self.public_key = (p, q, g, A)
        self.private_key = a
        if self.verbose:
            print("Prime p:\t\t\t", p)
            print("Prime q:\t\t\t", q)
            print("Generator g:\t\t", g)
            print("Private Key:\t\t", a)

    def gen_primes(self, L, N, seedlen):
        if seedlen < N:
            raise Exception("Length of seed too short!")
        start_time = time.time()
        h = hashlib.sha256()
        n = ceil(L / 256) - 1
        b = L - 1 - (n * 256)
        steps = 0
        while True:
            steps += 1
            # Step 5
            domain_parameter_seed = secrets.token_bytes(seedlen)
            domain_parameter_seed_int = int.from_bytes(domain_parameter_seed, "little")
            h.update(domain_parameter_seed)
            U = int(h.hexdigest(), 16) % pow(2, N - 1)
            h = hashlib.sha256()
            q = pow(2, N - 1) + U + 1 - (U % 2)
            if not isprime(q):
                continue
            offset = 1
            for counter in range(0, 4 * L):
                v_arr = []
                for j in range(0, n + 1):
                    hash_int = (domain_parameter_seed_int + offset + j)
                    h.update(hash_int.to_bytes(hash_int.bit_length() + 7 // 8, 'little'))
                    v_j = int(h.hexdigest(), 16)
                    h = hashlib.sha256()
                    v_arr.append(v_j)
                W = 0
                for i, v in enumerate(v_arr[:-1]):
                    W += v * pow(2, i * 256)
                W += (v_arr[len(v_arr) - 1] % (pow(2, b))) * pow(2, n * 256)
                X = W + pow(2, L - 1)
                c = X % (2 * q)
                p = X - (c - 1)
                if p < pow(2, L - 1) or not isprime(p):
                    offset += n + 1
                    continue
                else:
                    end_time = time.time()
                    if self.verbose:
                        print("Counter:\t\t\t", counter)
                        print("Steps:\t\t\t\t", steps)
                        print("Prime number\ngeneration time:\t", (end_time - start_time))
                    return p, q

    def create_generator(self, p, q):
        e = (p - 1) // q
        g = 1
        while g == 1:
            h = random.randint(1, p - 1)
            g = pow(h, int(e), p)
        return g

    def throw_prime_error(self):
        raise Exception("Prime size not supported")

    def check_parameter(self):
        if self.seedlen <= 0:
            self.seedlen = self.N
            print("┌─────────────────────────────┐\n"
                  "│         Warning!            │\n"
                  "│Length of seed too short     │\n"
                  "│Proceeding with value of N...│\n"
                  "└─────────────────────────────┘")
        match self.L:
            case 512:
                print("This key size is obsolete.\nUse only for testing!")
                if self.N != 160:
                    self.throw_prime_error()
            case 1024:
                if self.N != 160:
                    self.throw_prime_error()
            case 2048:
                if self.N != 224 and self.N != 256:
                    self.throw_prime_error()
            case 3072:
                if self.N != 256:
                    self.throw_prime_error()
            case _:
                raise Exception("Prime size not supported")

    def validate_generator(self, p, q, g):
        if not (2 <= g <= (p - 1)):
            return False
        if pow(g, q, p) == 1:
            return True
        return False

    def hash(self, m):
        h = sha256()
        h.update(m.encode('utf-8'))
        return int(h.hexdigest(), 16)

    def sign(self, m):
        if self.public_key is None or self.private_key is None:
            raise Exception("Keys have to be generated first")
        p = self.public_key[0]
        q = self.public_key[1]
        g = self.public_key[2]
        a = self.private_key
        time_start = time.process_time()

        k = random.randint(1, q)

        r = pow(g, k, p) % q
        h = self.hash(m)
        s = (mod_inverse(k, q) * (h + a * r)) % q
        time_end = time.process_time()
        if self.verbose:
            print("Signature\ngeneration time:\t", (time_end - time_start))
        return r, s

    def verify(self, m, r, s):
        p = self.public_key[0]
        q = self.public_key[1]
        g = self.public_key[2]
        A = self.public_key[3]
        if not 1 <= r <= q - 1 or not 1 <= s <= q - 1:
            return False
        time_start = time.process_time()
        h = self.hash(m)
        s_inv = mod_inverse(s, q)
        x = h * s_inv % q
        y = r * s_inv % q
        r2 = ((pow(g, x, p) * pow(A, y, p)) % p) % q
        time_end = time.process_time()
        if self.verbose:
            print("\nSignature\nverify time:\t\t", (time_end - time_start))
        return r == r2
